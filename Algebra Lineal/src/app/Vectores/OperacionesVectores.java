/**
 * OPERACIONES ENTRE VECTORES
*@author JAIME MENA, ALEX RAMIRES, MARCELO VARON,JUAN SEBASTIAN
* @version 25/07/2019
* @deprecated Este programa realiza operaciones entre vectores
* 
* */

        
package app.Vectores;
/**
 * Importamos java swing para mostrar en pantalle JOptionPane.ShowMessageDialog
 * 
 * */
import app.GUI.InicioGUI;
import javax.swing.JOptionPane;

/**
 * Clase Principal
 * @author Usuario
 */
public class OperacionesVectores {

    /**
     * inivializamos el main
     * @param args 
     */
    public static void main(String[] args) {
        /**
         * Declaramos las variables que almacenaran el vector como arrais
         */
        int vec1[]=new int[3];
        int vec2[]=new int[3];
        int vec3[]=new int[3];
        
        /**
         * declaramos una variable de tipo bollean para indicar la salida
         */
        boolean salir = false;
        /**
         * declaramos la variable que manejara la opcion de realizar una opcion
         */
        int op=0;
        
        /**
         * inicializamos el menu como string
         */
        String menu="***** Menú operaciones de vectores *****\n";
        menu+="1.- Llenar vectores\n";
        menu+="2.- Suma de vectores\n";
        menu+="3.- Resta de vectores\n";
        menu+="4.- Multiplicación de vectores\n";
        menu+="5.- Norma de un vector\n";
        menu+="6.- Salir\n";
        menu+="Digita una opción";
        
        /**
         * hacemos el menu con un do While
         */
        do{
            /**
             * pedimos un valor para la opcion 
             * 
             */
            op=Integer.parseInt(JOptionPane.showInputDialog(menu));
            
            /**
             * hacemos el menu con un switch
             */
            switch (op){
                
                /**
                 * en este caso llenamos el vector pidiendos los datos por pantalla
                 */
                
                case 1:
                    
                    for(int i=0;i<vec1.length;i++){
                        vec1[i]=Integer.parseInt(JOptionPane.showInputDialog("Digita el valor para vec1"+"["+i+"]"));
                    }
                    for(int i=0;i<vec2.length;i++){
                        vec2[i]=Integer.parseInt(JOptionPane.showInputDialog("Digita el valor para vec2"+"["+i+"]"));
                    }
                    /**
                     * Imprime los vectores
                     
                     **/
                    String cad1="", cad2="";
                    for(int i=0;i<vec1.length;i++){
                        cad1+=vec1[i]+" ";
                        cad2+=vec2[i]+" ";
                    }
                    System.out.println("vec1: "+ "< "+cad1+">");
                    System.out.println("vec2: "+"< "+cad2+">");
                    break;
                case 2:
                    /**Suma de vectores
                    
                    * */
                    
                    int variable=0;
                    System.out.println("La suma es de: ");
                    for(int i=0;i<vec1.length;i++){
                    //    vec3[i]=vec1[i]+vec2[i];
                       variable = vec1[i]+vec2[i];
                        System.out.print(variable+" ");
                        
                    }
                    System.out.println();
                   
                    break;
                case 3:
                    //Resta de vectores                    
                    System.out.println("La resta es de: ");
                    for(int i=0;i<vec1.length;i++){
                        //vec3[i]=vec1[i]-vec2[i];
                        int variable1 = vec1[i]-vec2[i];
                        System.out.print(variable1+" ");
                    }
                    System.out.println();              
                    break;
                case 4:
                    //Multiplicación de vectores
                    System.out.println("La multiplicación es de: ");
                    for(int i=0;i<vec1.length;i++){
                        //vec3[i]=vec1[i]*vec2[vec2.length-(i+1)];
                        int variable2 = vec1[i]*vec2[i];
                        System.out.print(variable2+" ");
                    }
                    System.out.println();                    
                    break;
                    
                case 5: { // Norma de un vector
                    double norma = 0;
                    double suma = 0;                   
                    for (int i=0; i<vec1.length;i++) {
                        suma += Math.pow(vec1[i], 2);                        
                    }
                    norma = Math.sqrt(suma);
                    System.out.println("La norma del vector numero 1: "+norma);
                    norma = 0;
                    suma = 0;
                    for (int i=0; i<vec2.length;i++) {
                        suma += Math.pow(vec2[i], 2);                        
                    }
                    norma = Math.sqrt(suma);
                    System.out.println("La norma del vector numero 2: "+norma);
                    break;
                }
                
                case 6: { // Salir
                    salir = true;
                    InicioGUI inicio=new InicioGUI();
                    inicio.setVisible(true);
                    break;
                }
            }
        }while(!salir);
    }
}