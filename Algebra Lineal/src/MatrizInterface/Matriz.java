package MatrizInterface;

import java.util.ArrayList;

public class Matriz {
    
    private String pasosDeterminante = "";
    private String pasosCofactores = "";
    private String pasosAdjunta = "";
    private String pasosInversa = "";
    private String pasosTranspuesta = "";
    private String MensajeError = "";
    
    public String getPasosDeterminante() {
        return pasosDeterminante;
    }
    
    public String getPasosCofactores() {
        return pasosCofactores;
    }
    
    public String getPasosAdjunta() {
        return pasosAdjunta;
    }
    
    public String getPasosInversa() {
        return pasosInversa;
    }
    
    public String getPasosTranspuesta()   {
        return pasosTranspuesta;
    }

    public String getMensajeError() {
        return MensajeError;
    }

    public void setPasosDeterminante(String pasosDeterminante) {
        this.pasosDeterminante = pasosDeterminante;
    }

    public void setPasosAdjunta(String pasosAdjunta) {
        this.pasosAdjunta = pasosAdjunta;
    }

    public void setPasosInversa(String pasosInversa) {
        this.pasosInversa = pasosInversa;
    }

    public void setPasosCofactores(String pasosCofactores) {
        this.pasosCofactores = pasosCofactores;
    }
    
    
    public int det2x2(ArrayList<ArrayList<Integer>> matriz) {
        pasosDeterminante += "(("+matriz.get(0).get(0) +"*"+ matriz.get(1).get(1)+") - ("+matriz.get(0).get(1) +"*"+ matriz.get(1).get(0)+"))";
        return ((matriz.get(0).get(0) * matriz.get(1).get(1)) - (matriz.get(0).get(1) * matriz.get(1).get(0)));
    }
    
    public ArrayList<ArrayList<Integer>> menor(ArrayList<ArrayList<Integer>> m) {

        ArrayList<ArrayList<Integer>> retorno = new ArrayList<ArrayList<Integer>>(m.size() - 1);
        for(int i=1; i < m.size(); i++) {
            
            ArrayList<Integer> temporal = new ArrayList<Integer>(retorno.size());

            for(int j=1; j < m.size(); j++) {
                temporal.add(m.get(i).get(j));
            }
            retorno.add(temporal);
        }

        return retorno;
    }
    
    public ArrayList<Integer> transformarVector(ArrayList<Integer> v, int pos) {

        ArrayList<Integer> nuevo = new ArrayList<Integer>(v.size());
        nuevo.add(v.get(pos));
        for(int i = 0; i < v.size(); i++){
            if(i != pos){
                nuevo.add(v.get(i));
            }
        }
        return nuevo;
    }
    
    public ArrayList<ArrayList<Integer>> cambiarColumnasMatriz(ArrayList<ArrayList<Integer>> m, int numero) {

        ArrayList<ArrayList<Integer>> nueva = new ArrayList<ArrayList<Integer>>(m.size());
        for(int i=0; i < m.size(); i++) {
            nueva.add(transformarVector(m.get(i), numero));
        }
        return nueva;
    }
    
    public int determinante(ArrayList<ArrayList<Integer>> matriz) {

        int d = 0;
        int multiplicador = 1;
        int contador = 1;

        if(matriz.size() == 2) {
            d = det2x2(matriz);
        } else {
            ArrayList<ArrayList<Integer>> matrizOriginal = matriz;
            int tam = matriz.size();
            for(int i=0; i < tam; i++) {
                pasosDeterminante += "("+multiplicador+") ("+matriz.get(0).get(0)+") (";
                d += (multiplicador * matriz.get(0).get(0) * determinante(menor(matriz)));
                
                if(contador != tam) {
                    matriz = cambiarColumnasMatriz(matrizOriginal, contador);
                }
                multiplicador *= -1;
                contador++;
                pasosDeterminante += ") + \n";
            }
           
        }
        return d;
    }
    
    public ArrayList<ArrayList<Integer>> cambiarFilaMatriz(ArrayList<ArrayList<Integer>> m, int posicion) {

        ArrayList<ArrayList<Integer>> nueva = new ArrayList<ArrayList<Integer>>(m.size());
       
        nueva.add(m.get(posicion));
        
        for(int i=0; i < m.size(); i++) {
            if(i != posicion) {
                nueva.add(m.get(i));                
            }                        
        }
        return nueva;
    }
    
    public ArrayList<ArrayList<Integer>>  transformarMatriz(ArrayList<ArrayList<Integer>> n, int fila, int columna) { 
        
        n = cambiarColumnasMatriz(n, columna);
        n = cambiarFilaMatriz(n, fila);      
        
        return n;
    }
    
    
    public int cofactor(ArrayList <ArrayList<Integer>> matriz, int fila, int columna) {
         
        int multiplicador = (int) Math.pow(-1, (fila+columna));
        int resultado = 0;
        resultado += (multiplicador * (determinante(menor(transformarMatriz(matriz, fila, columna)))));
        
        return resultado;
    }
    
    
    public ArrayList <ArrayList<Integer>> matrizCofactores(ArrayList <ArrayList<Integer>> matriz) {
        
        pasosCofactores = "";
        ArrayList <ArrayList<Integer>> nueva = new ArrayList<ArrayList<Integer>>(matriz.size());
        
        for(int i=0; i<matriz.size(); i++) {
            ArrayList <Integer> auxiliar = new ArrayList<>();
            for(int j=0; j<matriz.size(); j++){
                pasosCofactores += "Cofactor ["+i+"]["+j+"] = " + cofactor(matriz, i, j) + "\n";
                auxiliar.add(cofactor(matriz, i, j));
            }
            nueva.add(auxiliar);
         } 
        
        return nueva;
    }
    
    public ArrayList<ArrayList<Integer>> transpuesta(ArrayList<ArrayList<Integer>> matriz) {
                 
        pasosTranspuesta = "";
        ArrayList<ArrayList<Integer>> matrizTrans = new ArrayList<ArrayList<Integer>>(matriz.size());                                       
        
        for(int i=0; i<matriz.size(); i++) {
            ArrayList <Integer> auxiliar = new ArrayList<>();
            for(int j=0; j<matriz.size(); j++) {                  
                auxiliar.add(matriz.get(j).get(i));                
                pasosTranspuesta += ("|" + matriz.get(j).get(i) + "|");
            }            
            pasosTranspuesta += "\n";
            matrizTrans.add(auxiliar);
        }
        
        return matrizTrans;
    }
    
    public ArrayList<ArrayList<Integer>> adjunta(ArrayList<ArrayList<Integer>> matriz) {                
        
        matriz = matrizCofactores(matriz);
        matriz = transpuesta(matriz);
        
        return matriz;
        
    }
    
    public ArrayList<Integer> productoCruz(ArrayList<ArrayList<Integer>> matriz) {

        ArrayList<Integer> d = new ArrayList<>(matriz.size());
        int multiplicador = 1;
        int contador = 1;

        ArrayList<ArrayList<Integer>> matrizOriginal = matriz;
        int tam = matriz.size();

        for (int i = 0; i < tam; i++) {

            d.add(multiplicador * determinante(menor(matriz)));
            if (contador != tam) {
                matriz = cambiarColumnasMatriz(matrizOriginal, contador);
            }
            multiplicador *= -1;
            contador++;
        }

        return d;
    }
    
    public int productoPunto(ArrayList<Integer> vector, ArrayList <Integer>vector2) {
        
        int resultado = 0;
        int temporal = 0;      
        if(vector.size() != vector2.size()) {
            System.out.println("Error! Los Vectores no estan en dimensiones ");
        }else {
            System.out.println("\tLa Operacion se calcula de la siguiente manera \n\n");
            for(int i=0; i<vector.size(); i++) {
                temporal = vector.get(i) * vector2.get(i);
                System.out.println("PASO #"+(i+1)+": "+vector.get(i)+" * "+vector2.get(i)+" -Resultado Temporal: "+temporal);
                System.out.println("\t\t\t- Numero anterior: "+resultado);
                resultado = resultado + (vector.get(i) * vector2.get(i));                
                System.out.println("\t\t\t- Resultado Actual: "+resultado);
                System.out.println("");
            }
        }
        
        return resultado;
    }
    
    public ArrayList<ArrayList<Float>> cocienteParaAdjunta(ArrayList<ArrayList<Integer>> matriz, int nro) {
        
        ArrayList<ArrayList<Float>> nuevaMatriz = new ArrayList<>();
         
        for(int i=0; i<matriz.size(); i++) {
            ArrayList<Float> auxiliar = new ArrayList<>();
            for(int j=0; j<matriz.size(); j++) {
                float resultado = ((float)matriz.get(i).get(j)) / ((float)nro);
                pasosInversa += ("\n[ " + matriz.get(i).get(j) + " / " + nro + " } = " + resultado + "\n");                
                auxiliar.add(resultado);
            }
            nuevaMatriz.add(auxiliar);
        }
        
        return nuevaMatriz;
    }            
    
    public ArrayList<ArrayList<Float>> matrizInversa(ArrayList<ArrayList<Integer>> matriz) {
        
        int nroDeterminante = determinante(matriz);
        ArrayList<ArrayList<Float>> matrizNueva = new ArrayList<>();
        
        if(nroDeterminante == 0) {           
            MensajeError = "";
            System.out.println("La matriz dado no tiene inversa: "+nroDeterminante); 
            MensajeError += "La matriz dada no tiene inversa ya que el determinante es cero \n"; 
        }else {
            matriz = adjunta(matriz);
            matrizNueva = cocienteParaAdjunta(matriz, nroDeterminante);
        }
        
        return matrizNueva;
    }
}
