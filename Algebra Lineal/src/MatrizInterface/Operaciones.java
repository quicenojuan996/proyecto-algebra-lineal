/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MatrizInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author juanm
 */
public class Operaciones extends JFrame
{
    private Matriz operaciones;
    private ArrayList<ArrayList<Integer>> matriz;
    JTextArea resultado = new JTextArea();
    JButton Determinantes = new JButton("Determinantes");
    JButton Inversa = new JButton("Inversa");
    JButton Cofactores = new JButton("Cofactores");
    JButton Adjunta = new JButton("Adjunta");
    
    public Operaciones()
    {
        this.operaciones = new Matriz();
        this.matriz = new ArrayList<>();
        JScrollPane pupila = new JScrollPane(resultado);
        pupila.setBounds(330, 45, 200, 250);
        resultado.setEditable(false);
        
        
        Determinantes.setBounds(50, 50, 150, 35);
        Inversa.setBounds(50, 100, 150, 35);
        Cofactores.setBounds(50, 150, 150, 35);
        Adjunta.setBounds(50, 200, 150, 35);
        
        
        this.setLayout(null);
        getContentPane().add(pupila);
        getContentPane().add(Determinantes);
        getContentPane().add(Inversa);
        getContentPane().add(Cofactores);
        getContentPane().add(Adjunta);
        
        ActionListener oyente = new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                if(e.getSource().equals(Determinantes))
                {
                    mostrarDeterminante();
                } else if(e.getSource().equals(Inversa))
                {
                   mostrarInversa();
                } else if(e.getSource().equals(Cofactores))
                {
                    mostrarCofactores();
                } else if(e.getSource().equals(Adjunta))
                {
                    mostrarAdjunta();
                }
            }
        };
          Determinantes.addActionListener(oyente);
          Inversa.addActionListener(oyente);
          Cofactores.addActionListener(oyente);
          Adjunta.addActionListener(oyente);
        
    }
    
    public void Iniciar(ArrayList<ArrayList<Integer>> matrizw)
    {
        this.matriz = matrizw;
        this.setVisible(true);
        this.setSize(600,400);
        this.setTitle("Mi primera interfaz");
        resultado.setText("");
    }
    
    public void mostrarDeterminante()
    {
        operaciones.setPasosDeterminante("");
        resultado.setText("DETERMINANTE: \n\n");
        int determinante = operaciones.determinante(matriz);
        resultado.setText(resultado.getText() +operaciones.getPasosDeterminante());
        resultado.setText(resultado.getText() +"\nResultado: " + determinante + "\n");
    }
    
    public void mostrarInversa()
    {
        operaciones.setPasosInversa("");
        resultado.setText("INVERSA: \n\n");
        ArrayList<ArrayList<Float>> matrizInversa = operaciones.matrizInversa(matriz);
        ArrayList<ArrayList<Integer>> matrizCofactores = operaciones.matrizCofactores(matriz);
        if (operaciones.getMensajeError().equalsIgnoreCase("")) {
            String aux = "\n Realizamos transpuesta, es decir, pasar columnas a filas \n";
            resultado.setText(resultado.getText() +operaciones.getPasosCofactores() + mostrarMatriz(matrizCofactores) + aux + operaciones.getPasosTranspuesta() + operaciones.getPasosInversa());
            resultado.setText(resultado.getText() +"\nRESULTADO: \n" + mostrarMatrizAdjunta(matrizInversa) + "\n");
        }else {
            resultado.setText(resultado.getText() + operaciones.getMensajeError());
        }
    }
    
    public void mostrarCofactores()
    {
        operaciones.setPasosCofactores("");
        resultado.setText( "COFACTORES: \n\n");
        ArrayList<ArrayList<Integer>> matrizCofactores = operaciones.matrizCofactores(matriz);
        resultado.setText(resultado.getText() + operaciones.getPasosCofactores());

        resultado.setText(resultado.getText() +"\nResultado: \n" + mostrarMatriz(matrizCofactores) + "\n");
    }
    
    public void mostrarAdjunta()
    {
        operaciones.setPasosAdjunta("");
        resultado.setText( "ADJUNTA: \n\n");
        ArrayList<ArrayList<Integer>> matrizAdjunta = operaciones.adjunta(matriz);
        resultado.setText(resultado.getText() +operaciones.getPasosAdjunta());

        resultado.setText(resultado.getText() +"Resultado: \n" + mostrarMatriz(matrizAdjunta) + "\n");
    }

    public String mostrarMatriz(ArrayList<ArrayList<Integer>> m) {
        String mensaje = "";
        for (int i = 0; i < m.size(); i++) {
            mensaje += "|";
            for (int j = 0; j < m.size(); j++) {
                mensaje += m.get(i).get(j) + "|";
            }
            mensaje += "\n";
        }
        return mensaje;
    }

    public String mostrarMatrizAdjunta(ArrayList<ArrayList<Float>> m) {
        String mensaje = "";
        for (int i = 0; i < m.size(); i++) {
            mensaje += "|";
            for (int j = 0; j < m.size(); j++) {
                mensaje += m.get(i).get(j) + "|";
            }
            mensaje += "\n";
        }
        return mensaje;
    }
    
}
