/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MatrizInterface;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.plaf.IconUIResource;
import javax.swing.text.html.ImageView;

/**
 *
 * @author juanm
 */
public class Ventana extends JFrame {

    private ArrayList<ArrayList<Integer>> matriz;
    JLabel titulo = new JLabel("Instrucciones ...");
    JTextArea instrucciones = new JTextArea("");
    JTextArea matrizIngresada = new JTextArea("");
    JButton resolver = new JButton("Resolver");
    Operaciones operacioncitas = new Operaciones();
    JPanel Panel = new JPanel();

    public Ventana() 
    {
        getIconImage();
        this.setLocation(250, 150);
        matriz = new ArrayList<>();
        titulo.setBounds(60, 15, 150, 15);
        resolver.setBounds(407, 319, 86, 29);

        String matricita = "2,7,3" + "\n" + "4,9,3" + "\n" + "2,0,9";

        matrizIngresada.setBounds(350, 56, 200, 229);
        matrizIngresada.setText(matricita);
        matrizIngresada.setBackground(Color.WHITE);
        matrizIngresada.setOpaque(true);

        String texto = "Cada una de las lineas representa\n"
                + "una fila y cada columna es separada\n" + "por comas.\n" + "\n"
                + "Nota: \n" + "Recuerda que la matriz debe ser\n" + "cuadrada.\n" + "\n"
                + "En el recuadro se deja una matriz de \n" + "ejemplo";

        instrucciones.setBounds(40, 56, 250, 229);
        instrucciones.setText(texto);
        instrucciones.setEditable(false);
        instrucciones.setBackground(Color.WHITE);
        instrucciones.setOpaque(true);

        Panel.setLayout(null);
        atras();
        Panel.add(titulo);
        Panel.add(instrucciones);
        Panel.add(matrizIngresada);
        Panel.add(resolver);

        getContentPane().add(Panel);

        ActionListener oyente = new ActionListener() // clase abstracta
        {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource().equals(resolver)) {
                    construirMatriz();
                    if (validarMatriz()) {
                        operacioncitas.Iniciar(matriz);
                    } else {
                        JOptionPane.showMessageDialog(null, "La matriz no es cuadrada", "Algebra Lineal", JOptionPane.ERROR_MESSAGE);
                        matriz.clear();
                    }

                    
                }
            }
        };
        resolver.addActionListener(oyente);
        
        
    }

    public static void main(String[] args) {
        /*Ventana v = new Ventana();

        WindowAdapter wa = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };

        v.addWindowListener(wa);
        v.setVisible(true);
        v.setSize(600, 400);*/
        //v.setTitle("Mi primera interfaz");
    }

    public void construirMatriz() {
        matriz.clear();
        String datoMatriz = matrizIngresada.getText();

        StringTokenizer filaCompleta = new StringTokenizer(datoMatriz, "\n");

        while (filaCompleta.hasMoreElements()) {
            StringTokenizer elementoFila = new StringTokenizer(filaCompleta.nextToken(), ",");
            ArrayList<Integer> fila = new ArrayList<>();

            while (elementoFila.hasMoreElements()) {
                int elemento = Integer.parseInt(elementoFila.nextToken());
                fila.add(elemento);
            }

            matriz.add(fila);
        }
    }

    public boolean validarMatriz() {
        boolean bandera = true;

        for (int i = 0; i < matriz.size(); i++) {
            if (matriz.size() != matriz.get(i).size()) {
                bandera = false;
            }

        }
        return bandera;
    }
   
    @Override
     public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("imagenes/GUI/grafico.png"));
        return retValue;
    }
     
    public void atras() {
        JLabel atras = new javax.swing.JLabel();

        atras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/GUI/espalda64.png"))); 
        atras.setBounds(0, 0, 12, 12);
        //this.Panel.add(atras);
        
    }

}
