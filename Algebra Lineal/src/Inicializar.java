//package app.GUI;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Inicializar extends Application
{
    private Stage primaryStage;
    private BorderPane rootLayout;
    
    @Override
    public void start(Stage stage)throws Exception
    {
        primaryStage = stage;
        primaryStage.setTitle("OPERACIONES CON VECTORES");
        initRootLayout();   
    }
    
    public void initRootLayout()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Inicializar.class.getResource("/Vectores.fxml"));
            rootLayout = (BorderPane)loader.load();
            
            Scene scene = new Scene(rootLayout);
            
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            
            VectorController controlador = loader.getController();
            controlador.setAdmin(this);
            
            primaryStage.show();
            
        }catch(IOException ex)
        {
            System.out.println("Error en el sistema: " + ex.toString());
        }
    }
    
    public static void main(String[] args)
    {
        launch(args);
    }
    
    
    
}

