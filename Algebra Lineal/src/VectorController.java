
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.URL;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Usuario
 */
public class VectorController implements Initializable 
{

    private int numeroComponentes=0;
    private int vec1[];
    private int vec2[];
    private int vecR[];
    private int escVec1;
    private int escVec2;
    
    private Inicializar admin;
    @FXML
    private TextArea vector1;
    @FXML
    private TextArea vector2;
    @FXML
    private MenuButton opeVec2;
    @FXML
    private MenuItem normaVec2;
    @FXML
    private MenuItem anguloVec2;
    @FXML
    private MenuItem MulEscalarVec2;
    @FXML
    private MenuItem cosDirVec2;
    @FXML
    private MenuItem suma;
    @FXML
    private MenuItem resta;
    @FXML
    private MenuItem multiplicacion;
    @FXML
    private MenuItem anguloXVec;
    @FXML
    private MenuItem productoP;
    @FXML
    private TextArea escalar1;
    @FXML
    private TextArea escalar2;
    @FXML
    private TextArea Procedimiento;
    @FXML
    private MenuButton opVec3;
    @FXML
    private MenuItem normaVec21;
    @FXML
    private MenuItem anguloVec1;
    @FXML
    private MenuItem MulEscalarVec1;
    @FXML
    private MenuItem cosDirVec1;

    public Inicializar getAdmin() {
        return admin;
    }

    public void setAdmin(Inicializar admin) {
        this.admin = admin;
    }
    public TextArea getVector1() {
        return vector1;
    }

    public void setVector1(TextArea vector1) {
        this.vector1 = vector1;
    }

    public int getNumeroComponentes() {
        return numeroComponentes;
    }

    public void setNumeroComponentes(int numeroComponentes) {
        this.numeroComponentes = numeroComponentes;
    }
    
    

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
    public void obVec1()
    {
        String vecTem=vector1.getText();
        
        StringTokenizer token=new StringTokenizer(vecTem, ",");
        numeroComponentes=token.countTokens();
        this.vec1 = new int[numeroComponentes];
       
        while (token.hasMoreElements()) 
        {  
            for (int i = 0; i < numeroComponentes; i++) 
            {
               vec1[i]=Integer.parseInt(token.nextToken(","));
            }
        }
    }
    
    public void obVec2()
    {
        String vecTem=vector2.getText();
        
        StringTokenizer token=new StringTokenizer(vecTem, ",");
        numeroComponentes=token.countTokens();
        
        this.vec2 = new int[numeroComponentes];
        
        while (token.hasMoreElements()) 
        {  
            for (int i = 0; i <numeroComponentes; i++) 
            {
               vec2[i]=Integer.parseInt(token.nextToken(","));
            }
        }
    }
    
    public void obEscVec1()
    {
        String num=escalar1.getText();
        escVec1=Integer.parseInt(num);
      
    }
    
    public void obEscVec2()
    {
        String num=escalar2.getText();
        escVec2=Integer.parseInt(num);
        
    }
    
    @FXML
    public void suma()
    {
        obVec1();
        obVec2();
        String s="";
        String r="";
        
        this.vecR = new int[numeroComponentes];
        
            if (vec1.length==vec2.length) 
            {
                for (int i = 0; i < numeroComponentes; i++) 
            {
                vecR[i]=vec1[i]+vec2[i];
            }
            
            String cad1="";
            
                for(int i=0;i<vecR.length;i++)
                {
                    cad1+=vecR[i]+",";
                }
                
                s+="PROCEDIMIENTO\n";
                
                    for (int i = 0; i < numeroComponentes; i++) 
                    {
                        s+=""+vec1[i]+" + "+vec2[i]+" = "+vecR[i]+"\n";


                    }
                    
                    r+="\n Vector Resuntante:\n "+ "< "+cad1+">"+"\n";
                    Procedimiento.setText(s+r);
            } else  
            {
                Procedimiento.setText("LOS VECTORES DEBEN TENER LA MISMA CANTIDAD DE COMPONENTES.\n "
                                         + "CORRIJA SUS DATOS.");
            }
        
        
    }
    
    @FXML
    public void resta()
    {
        obVec1();
        obVec2();
        String s="";
        String r="";
        
        this.vecR = new int[numeroComponentes];
        
            if (vec1.length==vec2.length) 
            {
                for (int i = 0; i < numeroComponentes; i++) 
            {
                vecR[i]=vec1[i]-vec2[i];
            }
            
            String cad1="";
            
                for(int i=0;i<vecR.length;i++)
                {
                    cad1+=vecR[i]+",";
                }
                
                s+="PROCEDIMIENTO\n";
                
                    for (int i = 0; i < numeroComponentes; i++) 
                    {
                        s+=""+vec1[i]+" - "+vec2[i]+" = "+vecR[i]+"\n";


                    }
                    
                    r+="\n Vector Resuntante:\n "+ "< "+cad1+">"+"\n";
                    Procedimiento.setText(s+r);
            } else  
            {
               Procedimiento.setText("LOS VECTORES DEBEN TENER LA MISMA CANTIDAD DE COMPONENTES.\n "
                                         + "CORRIJA SUS DATOS.");
            }
       
    }
    
    @FXML
    public void multiplicacion()
    {
        obVec1();
        obVec2();
        String s="";
        
        try {
            if (vec1.length==1) 
        {   
            double X1=vec1[0];
            double Y1=vec2[0];
            double num=(X1*Y1);
            s+="PROCEDIMIENTO\n";
            s+="("+X1+" X "+Y1+")"+" = "+num+"\n";
            s+="El Resultado es: "+num;
            
            Procedimiento.setText(s);
        }else
        {
            if (vec1.length==2) 
            {
                double X1=vec1[0];
                double Y1=vec2[0];
                double X2=vec1[1];
                double Y2=vec2[1];
                double num=(X1*Y1+X2*Y2);
                s+="PROCEDIMIENTO\n";
                s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" = "+num+"\n";
                s+="El Resultado es: "+num;
                
                Procedimiento.setText(s);
            } else 
            {
                if (vec1.length==3) 
                {
                    double X1=vec1[0];
                    double Y1=vec2[0];
                    double X2=vec1[1];
                    double Y2=vec2[1];
                    double X3=vec1[2];
                    double Y3=vec2[2];
                    double num=(X1*Y1+X2*Y2+X3*Y3);
                    
                    s+="PROCEDIMIENTO\n";
                    s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" + "+"("+X3+" X "+Y3+")"+" = "+num+"\n";
                    s+="El Resultado es: "+num;
                
                    Procedimiento.setText(s);
                } else 
                {
                    if (vec1.length==4) 
                    {
                        double X1=vec1[0];
                        double Y1=vec2[0];
                        double X2=vec1[1];
                        double Y2=vec2[1];
                        double X3=vec1[2];
                        double Y3=vec2[2];
                        double X4=vec1[3];
                        double Y4=vec2[3];
                        double num=(X1*Y1+X2*Y2+X3*Y3+X4*Y4);
                        
                        s+="PROCEDIMIENTO\n";
                        s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" + "+"("+X3+" X "+Y3+")"+" + "+"("+X4+" X "+Y4+")"+" = "+num+"\n";
                        s+="El Resultado es: "+num;

                        Procedimiento.setText(s);
                    } else 
                    {
                        if (vec1.length==5) 
                        {
                            double X1=vec1[0];
                            double Y1=vec2[0];
                            double X2=vec1[1];
                            double Y2=vec2[1];
                            double X3=vec1[2];
                            double Y3=vec2[2];
                            double X4=vec1[3];
                            double Y4=vec2[3];
                            double X5=vec1[4];
                            double Y5=vec2[4];
                            double num=(X1*Y1+X2*Y2+X3*Y3+X4*Y4+X5*Y5);

                            s+="PROCEDIMIENTO\n";
                            s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" + "+"("+X3+" X "+Y3+")"+" + "+"("+X4+" X "+Y4+")"+" + "+"("+X5+" X "+Y5+")"+" = "+num+"\n";
                            s+="El Resultado es: "+num;

                            Procedimiento.setText(s);
                        } else 
                        {
                            if (vec1.length==6) 
                            {
                    
                                double X1=vec1[0];
                                double Y1=vec2[0];
                                double X2=vec1[1];
                                double Y2=vec2[1];
                                double X3=vec1[2];
                                double Y3=vec2[2];
                                double X4=vec1[3];
                                double Y4=vec2[3];
                                double X5=vec1[4];
                                double Y5=vec2[4];
                                double X6=vec1[5];
                                double Y6=vec2[5];
                                double num=(X1*Y1+X2*Y2+X3*Y3+X4*Y4+X5*Y5+X6*Y6);
                                s+="PROCEDIMIENTO\n";
                                s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" + "+"("+X3+" X "+Y3+")"+" + "+"("+X4+" X "+Y4+")"+" + "+"("+X5+" X "+Y5+")"+" + "+"("+X6+" X "+Y6+")"+" = "+num+"\n";
                                s+="El Resultado es: "+num;

                                Procedimiento.setText(s);                
                            }else
                            {
                                if (vec1.length==7) 
                                {
                    
                                    double X1=vec1[0];
                                    double Y1=vec2[0];
                                    double X2=vec1[1];
                                    double Y2=vec2[1];
                                    double X3=vec1[2];
                                    double Y3=vec2[2];
                                    double X4=vec1[3];
                                    double Y4=vec2[3];
                                    double X5=vec1[4];
                                    double Y5=vec2[4];
                                    double X6=vec1[5];
                                    double Y6=vec2[5];
                                    double X7=vec1[5];
                                    double Y7=vec2[5];
                                    double num=(X1*Y1+X2*Y2+X3*Y3+X4*Y4+X5*Y5+X6*Y6+X7*Y7);
                                    s+="PROCEDIMIENTO\n";
                                    s+="("+X1+" X "+Y1+")"+" + "+"("+X2+" X "+Y2+")"+" + "+"("+X3+" X "+Y3+")"+" + "+"("+X4+" X "+Y4+")"+" + "+"("+X5+" X "+Y5+")"+" + "+"("+X6+" X "+Y6+")"+" + "+"("+X7+" X "+Y7+")"+" = "+num+"\n";
                                    s+="El Resultado es: "+num;

                                    Procedimiento.setText(s);
                                }
                            }
                        } 
                    }   
                }
            }
        }
        } catch (Exception e) 
        {
            Procedimiento.setText("LOS VECTORES DEBEN TENER LA MISMA CANTIDAD DE COMPONENTES.\n "
                                     + "CORRIJA SUS DATOS.");            
            
        }
    }
    
    @FXML
    public void multXEscalar1()
    {
    
        try {
                obVec1();
                obEscVec1();

                String s="";
                String r="";

                this.vecR = new int[numeroComponentes];

                    for (int i = 0; i < numeroComponentes; i++) 
                    {
                        vecR[i]=vec1[i]*escVec1;
                    }

                    String cad1="";

                        for(int i=0;i<vecR.length;i++)
                        {
                            cad1+=vecR[i]+",";
                        }

                        s+="PROCEDIMIENTO\n";

                            for (int i = 0; i < numeroComponentes; i++) 
                            {
                                s+=""+vec1[i]+" x "+escVec1+" = "+vecR[i]+"\n";
                            }
                            r+="\n Vector Resuntante:\n "+ "< "+cad1+">"+"\n";
                            Procedimiento.setText(s+r);
            } catch (Exception e) 
        {
            Procedimiento.setText("ASEGURESE DE INGRESAR LOS DATOS CORRECTAMENTE.\n"
                                + "NO DEJE CAMPOS VACIOS."
                                + "SIGA LAS INSTRUCCIONES.");
        }
    }
    
    @FXML
    public void multXEscalar2()
    {
        try {
                obVec1();
                obEscVec2();

                String s="";
                String r="";

                this.vecR = new int[numeroComponentes];

                for (int i = 0; i < numeroComponentes; i++) 
                {
                    vecR[i]=vec2[i]*escVec2;
                }

                String cad1="";

                    for(int i=0;i<vecR.length;i++)
                    {
                        cad1+=vecR[i]+",";
                    }

                    s+="PROCEDIMIENTO\n";

                        for (int i = 0; i < numeroComponentes; i++) 
                        {
                            s+=""+vec2[i]+" x "+escVec2+" = "+vecR[i]+"\n";
                        }

                        r+="\n Vector Resuntante:\n "+ "< "+cad1+">"+"\n";
                        Procedimiento.setText(s+r);
 
            } catch (Exception e) 
            {
                Procedimiento.setText("ASEGURESE DE INGRESAR LOS DATOS CORRECTAMENTE.\n"
                                    + "NO DEJE CAMPOS VACIOS."
                                    + "SIGA LAS INSTRUCCIONES.");
            }
    
    
    }
    
    @FXML
    public void normaVec1()
    {
        obVec1();
        
        
        if (vec1.length==1) 
        {
            double num=vec1[0];
            num=Math.sqrt(num*num);
            
            Procedimiento.setText("||U||="+num+"u");
        }else
        {
            if (vec1.length==2) 
            {
                double num=vec1[0];
                double num2=vec1[1];
                num=Math.sqrt(num*num+num2*num2);
                
                Procedimiento.setText("||U||="+num+"u");
            }else 
            {
                if (vec1.length==3) 
                {
                    double num=vec1[0];
                    double num2=vec1[1];
                    double num3=vec1[2];
                    num=Math.sqrt(num*num+num2*num2+num3*num3);
                    
                    Procedimiento.setText("||U||="+num+"u");
                }else 
                {
                    if (vec1.length==4) 
                    {
                        double num=vec1[0];
                        double num2=vec1[1];
                        double num3=vec1[2];
                        double num4=vec1[3];
                        num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4);
                    
                        Procedimiento.setText("||U||="+num+"u");
                    } else 
                    {
                        if (vec1.length==5) 
                        {
                            double num=vec1[0];
                            double num2=vec1[1];
                            double num3=vec1[2];
                            double num4=vec1[3];
                            double num5=vec1[4];
                            num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5);

                            Procedimiento.setText("||U||="+num+"u");
                        } else 
                        {
                            if (vec1.length==5) 
                            {
                                double num=vec1[0];
                                double num2=vec1[1];
                                double num3=vec1[2];
                                double num4=vec1[3];
                                double num5=vec1[4];
                                double num6=vec1[5];
                                num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5+num6*num6);

                                Procedimiento.setText("||U||="+num+"u");
                            }else
                            {
                                if (vec1.length==7) 
                                {
                                    double num=vec1[0];
                                    double num2=vec1[1];
                                    double num3=vec1[2];
                                    double num4=vec1[3];
                                    double num5=vec1[4];
                                    double num6=vec1[5];
                                    double num7=vec1[6];
                                    num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5+num6*num6+num7*num7);

                                    Procedimiento.setText("||U||="+num+"u");
                                }
                            }
                        } 
                    }   
                }
            }
        }
    }
     
    @FXML
    public void normaVec2()
    {
        obVec2();
        
        
        if (vec2.length==1) 
        {
            double num=vec2[0];
            num=Math.sqrt(num*num);
            
            Procedimiento.setText("||V||="+num+"u");
        }else
        {
            if (vec2.length==2) 
            {
                double num=vec2[0];
                double num2=vec2[1];
                num=Math.sqrt(num*num+num2*num2);
                
                Procedimiento.setText("||V||="+num+"u");
            }else 
            {
                if (vec2.length==3) 
                {
                    double num=vec2[0];
                    double num2=vec2[1];
                    double num3=vec2[2];
                    num=Math.sqrt(num*num+num2*num2+num3*num3);
                    
                    Procedimiento.setText("||V||="+num+"u");
                }else 
                {
                    if (vec2.length==4) 
                    {
                        double num=vec2[0];
                        double num2=vec2[1];
                        double num3=vec2[2];
                        double num4=vec2[3];
                        num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4);
                    
                        Procedimiento.setText("||V||="+num+"u");
                    }else 
                   {
                        if (vec2.length==5) 
                        {
                            double num=vec2[0];
                            double num2=vec2[1];
                            double num3=vec2[2];
                            double num4=vec2[3];
                            double num5=vec2[4];
                            num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5);
                    
                            Procedimiento.setText("||V||="+num+"u");
                        } else 
                        {
                            if (vec2.length==6) 
                            {
                                double num=vec2[0];
                                double num2=vec2[1];
                                double num3=vec2[2];
                                double num4=vec2[3];
                                double num5=vec2[4];
                                double num6=vec2[5];
                                num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5+num6*num6);
                    
                                Procedimiento.setText("||V||="+num+"u");
                            }else
                            {
                                if (vec2.length==7) 
                                {
                                    double num=vec2[0];
                                    double num2=vec2[1];
                                    double num3=vec2[2];
                                    double num4=vec2[3];
                                    double num5=vec2[4];
                                    double num6=vec2[5];
                                    double num7=vec2[6];
                                    num=Math.sqrt(num*num+num2*num2+num3*num3+num4*num4+num5*num5+num6*num6+num7*num7);
                    
                                    Procedimiento.setText("||V||="+num+"u");
                                }
                            }  
                        } 
                    }   
                }
            }
        }
    }
     
    @FXML
    public void anguloVec1()
    {
       obVec1();
       
       double num;
       String s="";
       
       
        
       if (vec1.length==1 ||vec1.length==2) 
       {
           double x=vec1[0];
           double y=vec1[1];
       
           if ((x < 0) && (y < 0) || (x<0)) 
           {
               num= Math.toDegrees( Math.atan(y/x)) + 180;
               s+="arcTan("+x+"/"+y+")"+" + "+"180°"+" = "+num+"°\n";
               s+="El Angulo Es: "+num+"°";
               Procedimiento.setText(s);
           }else
           {
               if(y < 0) 
               {
                    num=Math.toDegrees( Math.atan(y/x)) + 360;
                    
                    s+="arcTan("+x+"/"+y+")"+" + "+"360°"+" = "+num+"°\n";
                    s+="El Angulo Es: "+num+"°";
                    Procedimiento.setText(s);
	       }else
               {
                   num=Math.toDegrees( Math.atan(y/x)) ;
                   s+="arcTan("+x+"/"+y+")"+" + "+" = "+num+"°\n";
                   s+="El Angulo Es: "+num+"°";
                   Procedimiento.setText(s);
               }
           }
        }else
        {
            if (vec1.length==3) 
            {
                double x=vec1[0];
                double y=vec1[1];
                double z=vec1[2];
                double num1=Math.sqrt(x*x+y*y+y*y);
                double num5=Math.toDegrees( Math.atan(x /(num1)));
                double num6=Math.toDegrees( Math.atan(y /(num1)));
                double num7=Math.toDegrees( Math.atan(z /(num1)));
                s+="PROCEDIMIENTO \n\n";
                  
                s+="|U| = "+num1+"\n\n";
                    
                s+="arcCos( ( X ) / (||U||) )" +"°\n";
                s+="arcCos( ( Y ) / (||U||) )" +"°\n";
                s+="arcCos( ( Z ) / (||U||) )" +"°\n\n";
                    
                s+="arcCos( ("+x+") / ("+num1+") )" +" = "+num5+"°\n";
                s+="arcCos( ("+y+") / ("+num1+") )" +" = "+num6+"°\n";
                s+="arcCos( ("+z+") / ("+num1+") )" +" = "+num7+"°\n\n";
                    
                s+="El Angulo en X es: "+num5+"°\n";
                s+="El Angulo en Y es: "+num6+"°\n";
                s+="El Angulo en Z es: "+num7+"°\n";
                    
                Procedimiento.setText(s);
                
           }else
           {
               Procedimiento.setText("SOLO SE PUEDE CALCULAR EL ANGULO A VECTORES EN R3");
           }
       }
   }
   
    @FXML
    public void anguloVec2()
    {
       obVec2();
       double num;
       String s="";
       
       if (vec2.length==1 ||vec2.length==2) 
       {
           double x=vec2[0];
           double y=vec2[1];
       
           if (x < 0 && y < 0 || x<0) 
           {
               num= Math.toDegrees( Math.atan(y/x)) + 180;
               s+="arcTan("+x+"/"+y+")"+" + "+"180°"+" = "+num+"°\n";
               s+="El Angulo Es: "+num+"°";
               Procedimiento.setText(s);
           }else
           {
               if(y < 0) 
               {
                    num=Math.toDegrees( Math.atan(y/x)) + 360;
                    
                    s+="arcTan("+x+"/"+y+")"+" + "+"360°"+" = "+num+"°\n";
                    s+="El Angulo Es: "+num+"°";
                    Procedimiento.setText(s);
	       }else
               {
                   num=Math.toDegrees( Math.atan(y/x)) ;
                   s+="arcTan("+x+"/"+y+")"+" + "+" = "+num+"°\n";
                   s+="El Angulo Es: "+num+"°";
                   Procedimiento.setText(s);
               }
           }
        }else
        {
            if (vec2.length==3) 
            {
                
                double x=vec2[0];
                double y=vec2[1];
                double z=vec2[2];
                double num1=Math.sqrt(x*x+y*y+y*y);
                double num5=Math.toDegrees( Math.atan(x /(num1)));
                double num6=Math.toDegrees( Math.atan(y /(num1)));
                double num7=Math.toDegrees( Math.atan(z /(num1)));
                s+="PROCEDIMIENTO \n\n";
                  
                s+="|V| = "+num1+"\n\n";
                    
                s+="arcTan( ( X ) / (||V||) )" +"°\n";
                s+="arcTan( ( Y ) / (||V||) )" +"°\n";
                s+="arcTan( ( Z ) / (||V||) )" +"°\n\n";
                    
                s+="arcTan( ("+x+") / ("+num1+") )" +" = "+num5+"°\n";
                s+="arcTan( ("+y+") / ("+num1+") )" +" = "+num6+"°\n";
                s+="arcTan( ("+z+") / ("+num1+") )" +" = "+num7+"°\n\n";
                    
                s+="El Angulo en X es: "+num5+"°\n";
                s+="El Angulo en Y es: "+num6+"°\n";
                s+="El Angulo en Z es: "+num7+"°\n";
                    
                Procedimiento.setText(s);
           }else
           {
               Procedimiento.setText("SOLO SE PUEDE CALCULAR EL ANGULO A VECTORES EN R3");
           }
       }
   }
    

    @FXML
    public void anguloEntreVectores()
    {
        try {
                obVec1();
                obVec2();
                String s="";

                double X1=vec1[0];
                double Y1=vec1[1];
                double Z1=vec1[2];

                double X2=vec2[0];
                double Y2=vec2[1];
                double Z2=vec2[2];

                double num1=Math.sqrt(X1*X1+Y1*Y1+Z1*Z1);
                double num2=Math.sqrt(X2*X2+Y2*Y2+Z2*Z2);
                double num3=(X1*X2+Y1*Y2+Z1*Z2);


                double num= Math.toDegrees( Math.acos( ( num3)/(num1*num2)));
                s+="PROCEDIMIENTO \n\n";

                s+=" U.V = "+num3+"\n\n";
                s+="||U|| = "+num1+"\n\n";
                s+="||V|| = "+num2+"\n\n";

                s+="A° = arcCos( U.V /||U|| . ||V|| )\n\n";

                s+="A° = arcCos( "+num3+" /||"+num1+"|| . ||"+num2+"|| )\n\n";

                s+="A° = "+num+"°";


                 Procedimiento.setText(s);
            } catch (Exception e) 
            {
              Procedimiento.setText("LOS VECTORES DEBEN TENER LA MISMA CANTIDAD DE COMPONENTES.\n "
                                         + "CORRIJA SUS DATOS.");   
            }
    }
    
    @FXML
    public void cosDirVec1()
    {
        obVec1();
        String s="";
        double x=vec1[0];
        double y=vec1[1];
        double z=vec1[2];
        
            if (vec1.length==3) 
            {
                double num1=Math.sqrt(x*x+y*y+y*y);
                double num5=Math.toDegrees( Math.acos(x /(num1)));
                double num6=Math.toDegrees( Math.acos(y /(num1)));
                double num7=Math.toDegrees( Math.acos(z /(num1)));
                s+="PROCEDIMIENTO \n\n";
                  
                s+="|U| = "+num1+"\n\n";
                    
                s+="arcCos( ( X ) / (||U||) )" +"°\n";
                s+="arcCos( ( Y ) / (||U||) )" +"°\n";
                s+="arcCos( ( Z ) / (||U||) )" +"°\n\n";
                    
                s+="arcCos( ("+x+") / ("+num1+") )" +" = "+num5+"°\n";
                s+="arcCos( ("+y+") / ("+num1+") )" +" = "+num6+"°\n";
                s+="arcCos( ("+z+") / ("+num1+") )" +" = "+num7+"°\n\n";
                    
                s+="El Angulo en X es: "+num5+"°\n";
                s+="El Angulo en Y es: "+num6+"°\n";
                s+="El Angulo en Z es: "+num7+"°\n";
                    
                Procedimiento.setText(s);
                
           }else
           {
               Procedimiento.setText("SOLO SE PUEDE CALCULAR COSENOS DIRECTORES EN VECTORES EN R3");
           }
    }
  
    @FXML
    public void cosDirVec2()
    {
       obVec1();
       double num1;
       String s="";
       double x=vec2[0];
       double y=vec2[1];
       double z=vec2[2];
        if (vec2.length==3) 
            {
                
        
                 num1=Math.sqrt(x*x+y*y+y*y);
                double num5=Math.toDegrees( Math.acos(x /(num1)));
                double num6=Math.toDegrees( Math.acos(y /(num1)));
                double num7=Math.toDegrees( Math.acos(z /(num1)));
                s+="PROCEDIMIENTO \n\n";
                  
                s+="|V| = "+num1+"\n\n";
                    
                s+="arcCos( ( X ) / (||V||) )" +"°\n";
                s+="arcCos( ( Y ) / (||V||) )" +"°\n";
                s+="arcCos( ( Z ) / (||V||) )" +"°\n\n";
                    
                s+="arcCos( ("+x+") / ("+num1+") )" +" = "+num5+"°\n";
                s+="arcCos( ("+y+") / ("+num1+") )" +" = "+num6+"°\n";
                s+="arcCos( ("+z+") / ("+num1+") )" +" = "+num7+"°\n\n";
                    
                s+="El Angulo en X es: "+num5+"°\n";
                s+="El Angulo en Y es: "+num6+"°\n";
                s+="El Angulo en Z es: "+num7+"°\n";
                    
                Procedimiento.setText(s);
           }else
           {
               Procedimiento.setText("SOLO SE PUEDE CALCULAR EL ANGULO A VECTORES EN R3");
           }
    }

}
